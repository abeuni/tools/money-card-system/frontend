import Vue from 'vue';
import Router from 'vue-router';
import QRcode from '@/views/QRcode.vue';
import StorePage from '@/views/Store.vue';
import CardsPage from '@/views/listings/Cards.vue';
import ProductsPage from '@/views/listings/Products.vue';
import ProductGroupsPage from '@/views/listings/ProductGroups.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/qrcode/:dpt',
      name: 'qrcode',
      component: QRcode,
    },
    {
      path: '/',
      redirect: { name: 'cards' },
    },
    {
      path: '/cards',
      name: 'cards',
      component: CardsPage,
    },
    {
      path: '/products',
      name: 'products',
      component: ProductsPage,
    },
    {
      path: '/groups',
      name: 'groups',
      component: ProductGroupsPage,
    },
    {
      path: '/store/:group_id',
      name: 'store',
      component: StorePage,
    },
  ],
});
