import '@mdi/font/css/materialdesignicons.css';
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import enUS from '@/locales/en-US';
import VCurrencyField from 'v-currency-field';
import 'v-currency-field/dist/index.css';

Vue.use(Vuetify, {
  lang: {
    locales: { 'en-US': enUS },
    current: 'en-US',
  },
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
  },
  iconfont: 'mdi',
});

Vue.use(VCurrencyField);
